'use strict';

class PricesEditController {
	constructor(PricesService, $stateParams, $location) {
		this.PricesService = PricesService;
		this.$location = $location;
		this.prices = [];
		this.PricesService.showOne($stateParams.pricesId).then((data)=> {
			this.prices = data.data;
		});
	}

	updatePrice(){
		this.PricesService.update(this.prices).then(() => {
			this.$location.path('/prices')
		});
	}
}

angular.module('prices').controller('PricesEditController', PricesEditController);
