'use strict';

module.exports = function (app) {

	var core = require('../../app/controllers/core.server.controller');
	var jwt = require('jsonwebtoken');
	app.use('/api/v1',function(req, res, next){
		var token = req.body.token || req.query.token || req.headers['x-access-token'];
		if (token){
			jwt.verify(token, 'naviezwinti6', function(err, decoded){
				if (err){
					return res.json({success: false, message: 'Failed to authenticate token'});
				} else {
					req.decoded = decoded;
					next();
				}
			});
		} else {
			return res.status(403).send({success: false, message: 'No token provided'});
		}
	});

	app.route('/').get(core.index);
};
