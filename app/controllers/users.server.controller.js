'use strict';

/**
 * Module dependencies.
 */

var models = require('../../config/sequelize');
var jwt = require('jsonwebtoken');

exports.signUp = function(req, res, next){
	models.users.create(req.body).then(function(users){
		return res.status(200).send('User ' + users.email + ' successfully registered');
	}).catch(function (err){
		next(err.message);
	});
};

exports.signIn =  function(req, res, next){

	models.users.findOne({email: req.body.email}).then(function(user){
		if(user.password === req.body.password && user.email === req.body.email) {
			var token = jwt.sign(user.dataValues, 'naviezwinti6', {
				expiresIn: 300
			});
			console.log(token);
			res.json({
				user: req.body.email,
				success: true,
				message: "Authentication passed",
				token: token
			});
		} else res.send({message: 'Invalid email or password', success: false});
	}).catch(function(err){
		next(err.message);
	});


};
