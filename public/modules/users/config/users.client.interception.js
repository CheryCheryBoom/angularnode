'use strict';

angular.module('users').factory('tokenInjection', function () {
	this.tokenInjection = {
		request: function (req) {
			if (req.headers !== undefined) {
				req.headers['x-access-token'] = localStorage.getItem('token');
			}
			return req;
		}
	};
	return this.tokenInjection;
});
angular.module('users').config( function ($httpProvider) {
	$httpProvider.interceptors.push('tokenInjection');
});
