'use strict';

class PricesListController {
	// @ngInject
	constructor(PricesService, $state) {
		this.PricesService = PricesService;
		this.$state = $state;
		this.prices = [];
		this.isDeleted = [];
		PricesService.show().then((data) => {
			this.prices = data.data;
		});
	}

	deletePrice(id){
		this.PricesService.destroy(id).then(() => {
			this.isDeleted = true;
			this.$state.reload();
		});
	}

}

angular.module('prices').controller('PricesListController', PricesListController);
