'use strict';

module.exports = function(app){
	var prices = require('../../app/controllers/prices.server.controller.js');

	app.route('/api/v1/prices')
	.post(prices.create);

	app.route('/api/v1/prices')
	.get(prices.findAll);

	app.route('/api/v1/prices/:pricesId')
	.get(prices.findById);

	app.route('/api/v1/prices')
	.put(prices.update);

	app.route('/api/v1/prices/:pricesId')
	.delete(prices.destroy);
};
