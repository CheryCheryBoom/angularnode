'use strict';

module.exports = function(app){
	var users = require('../../app/controllers/users.server.controller.js');
	var jwt = require('jsonwebtoken');

	app.route('/signup')
	.post(users.signUp);


	app.post('/signin',users.signIn);

};
