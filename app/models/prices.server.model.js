'use strict';

module.exports = function(sequelize, DataTypes){
	return sequelize.define('prices', {
		id: {
			type: DataTypes.INTEGER,
			primaryKey: true,
			autoIncrement: true
		},
		name: {
			type: DataTypes.STRING(45),
			allowNull: false
		},
		value: {
			type: DataTypes.DOUBLE,
			allowNull: false
		}

	},{
		freezeTableName: true
	});
};
