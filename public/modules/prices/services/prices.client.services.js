'use strict';

class PricesService {
	constructor($http){
		this.$http = $http;
	}

	create(data){
		return this.$http.post('api/v1/prices', data);
	}

	show() {
		return this.$http.get('api/v1/prices')
	}

	showOne(id) {
		return this.$http.get('api/v1/prices/'+ id)
	}

	update(data){
		return this.$http.put('api/v1/prices', data)
	}

	static factory($http){
		return new PricesService($http);
	}

	destroy(id){
		return this.$http.delete('api/v1/prices/' + id)
	}


}

angular.module('prices').service('PricesService', PricesService.factory);
