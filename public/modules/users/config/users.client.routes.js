'use strict';

class UsersRoutes {
	constructor($stateProvider) {
		this.$stateProvider = $stateProvider;
		this.init();
	}

	init() {
		this.$stateProvider
			.state('signUp', {
				url: '/signup',
				controller: 'SignUpController',
				controllerAs: 'vm',
				templateUrl: '/modules/users/views/users-signup.client.view.html'
			})
			.state('signIn', {
				url: '/signin',
				controller: 'SignInController',
				controllerAs: 'vm',
				templateUrl: '/modules/users/views/users-signin.client.view.html'
			});
	}

	static factory($stateProvider) {
		return new UsersRoutes($stateProvider);
	}
}

angular.module('users').config(UsersRoutes.factory);
