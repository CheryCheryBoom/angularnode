'use strict';

class SignUpController {
	constructor(UsersService, $location){
		this.UsersService = UsersService;
		this.$location = $location;
		this.user = []
	}

	signUp(){
		this.UsersService.signUp(this.user).then(() => {
			this.$location.path('/');
		});
	}

}

angular.module('users').controller('SignUpController', SignUpController);
