'use strict';

class SignInController {
	constructor(UsersService){
		this.UsersService = UsersService;
		this.user = {};
	}

	signIn(){
		this.UsersService.signIn(this.user);
	}

}

angular.module('users').controller('SignInController', SignInController);
