'use strict';

class PricesCreateController {
    // @ngInject
    constructor(PricesService, $location) {
		this.PricesService = PricesService;
		this.$location = $location;
		this.prices = {};
    }

	sendPrices(){
		this.PricesService.create(this.prices).then(() => {
			this.$location.path('/prices');
		});
	}


}

angular.module('prices').controller('PricesCreateController', PricesCreateController);
