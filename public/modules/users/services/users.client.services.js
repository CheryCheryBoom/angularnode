'use strict';

class UsersService {
	constructor($http, $location) {
		this.$http = $http;
		this.token = '';
		this.$location = $location;
		this.authentication = '';
	}

	signUp(user) {
		return this.$http.post('/signup', user);
	}

	signIn(user) {
		 this.$http.post('/signin', user).then((data) => {
			if (data.data.success) {
				this.authentication = data.data.user;
				localStorage.setItem('token',data.data.token);
				this.token = localStorage.getItem('token');
				this.$location.path('/');
			} else {
				console.log(data.data.message);
			}
		});
	}

	signOut(){
		this.authentication = {};
		localStorage.removeItem('token');
		this.$location.path('/signin');
	}

	getUser() {
		return this.authentication;
	}

	checkAuthorization() {
		return !(this.authentication.length === 0 || !localStorage.getItem('token'));
	}

	static factory($http, $location) {
		return new UsersService($http, $location);
	}
}

angular.module('users').service('UsersService', UsersService.factory);
