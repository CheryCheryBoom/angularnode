'use strict';

class PricesRoutes {
	constructor($stateProvider) {
		this.$stateProvider = $stateProvider;
		this.init();
	}

	init() {
		this.$stateProvider
			.state('sidebar.pricesCreate', {
				url: '/prices/create',
				controller: 'PricesCreateController',
				controllerAs: 'vm',
				templateUrl: '/modules/prices/views/create-prices.client.view.html'
			})
			.state('sidebar.pricesList', {
				url: '/prices',
				controller: 'PricesListController',
				controllerAs: 'vm',
				templateUrl: '/modules/prices/views/list-prices.client.view.html'
			})
			.state('sidebar.pricesEdit', {
				url: '/prices/:pricesId',
				controller: 'PricesEditController',
				controllerAs: 'vm',
				templateUrl: '/modules/prices/views/edit-prices.client.view.html'
			});
	}

	// @ngInject
	static factory($stateProvider) {
		return new PricesRoutes($stateProvider);
	}
}

angular.module('prices').config(PricesRoutes.factory);
