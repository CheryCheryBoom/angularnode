'use strict';
var models = require('../../config/sequelize.js');

exports.create = function (req, res, next) {
	models.prices.create(req.body).then(function (prices) {
		return res.status(200).send(prices);
	}).catch(function (err) {
		next(err.message);
	});
};

exports.findAll = function (req, res, next) {
	models.prices.findAll().then(function (prices) {
		return res.json(prices);
	}).catch(function (err) {
		next(err.message);
	});
};

exports.findById = function (req, res, next) {
	models.prices.findById(req.params.pricesId).then(function (prices) {
		return res.status(200).send(prices);
	}).catch(function (err) {
		next(err.message);
	});
};

exports.update = function (req, res, next) {
	models.prices.update({
		name: req.body.name,
		value: req.body.value
	}, {
		where: {
			id: req.body.id
		}
	}).then(function (prices) {
		return res.status(200).send(prices);
	}).catch(function (err) {
		next(err.message);
	});
};

exports.destroy = function (req, res, next) {
	models.prices.destroy({
		where: {
			id: req.params.pricesId
		}
	}).then(function (rowDeleted){
		return res.sendStatus(200);
	}).catch(function (err){
		next(err.message);
	});
};
