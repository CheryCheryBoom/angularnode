'use strict';

class HeaderController {
	// @ngInject
/*	constructor(){

	}*/
	constructor(UsersService) {
		this.UsersService = UsersService;
	}

	signOut(){
		this.UsersService.signOut();
	}
}

angular.module('core').controller('HeaderController', HeaderController);
