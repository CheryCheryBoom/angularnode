'use strict';

module.exports = function (sequelize, DataTypes){
	return sequelize.define('users', {
		id: {
			type: DataTypes.INTEGER,
			primaryKey: true,
			autoIncrement: true
		},

		email: {
			type: DataTypes.STRING(45),
			allowNull: false
		},

		password: {
			type: DataTypes.STRING(45),
			allowNull: false
		}
	});
};
