'use strict';

class AccessRoutes {
	constructor($rootScope, UsersService, $state) {
		this.$rootScope = $rootScope;
		this.UsersService = UsersService;
		this.$state = $state;
		this.init();
	}

	init() {
		this.$rootScope.$on('$stateChangeStart', (event, toState) => {
			this.$rootScope.isAuthorized = this.UsersService.checkAuthorization();
			if(!this.UsersService.checkAuthorization()){
				if (toState.name.split('.')[0] === 'sidebar'){
					this.$state.go('signIn');
					return event.preventDefault();
				}
			}
		});
	}

	// @ngInject
	static factory($rootScope, UsersService, $state) {
		return new AccessRoutes($rootScope, UsersService, $state);
	}
}

angular.module('core').run(AccessRoutes.factory);
